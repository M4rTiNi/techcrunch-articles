import type { SetterOrUpdater } from 'recoil';

import type { GetArticlesReturnType, PaginationDataType } from '../../../api';
import type { ArticleQueryParams } from '../../../state/type';

export interface ArticleItemData {
  id: number;
  title: { rendered: string };
  date: string;
  link: string;
  author: string;
  include: number[];
}

export type ArticleListProps = {
  articleQuery: ArticleQueryParams;
  setArticleQuery: SetterOrUpdater<ArticleQueryParams>;
} & GetArticlesReturnType &
  PaginationDataType;

export type QueryType = Pick<ArticleListProps, 'setArticleQuery' | 'articleQuery'>;
