import React from 'react';
import { useRecoilState } from 'recoil';

import Layout from '../../../components/Layout';
import { favouriteArticleQueryKeysState } from '../../../state';
import useGetFavouriteArticles from '../hooks/useGetFavouriteArticles';

import ArticleList from './components/ArticleList';

const FavouriteArticles = () => {
  const [favouriteArticleQueryKeys, setFavouriteArticleQueryKeys] = useRecoilState(
    favouriteArticleQueryKeysState,
  );
  const { data } = useGetFavouriteArticles();

  const isSomeFavouritesData = !!favouriteArticleQueryKeys.include?.length;

  return (
    <Layout>
      {isSomeFavouritesData && (
        <ArticleList
          data={isSomeFavouritesData ? data?.data : []}
          paginationData={data?.paginationData}
          articleQuery={favouriteArticleQueryKeys}
          setArticleQuery={setFavouriteArticleQueryKeys}
        />
      )}
      {!isSomeFavouritesData && <p style={{ textAlign: 'center' }}>No favorites articles</p>}
    </Layout>
  );
};

export default FavouriteArticles;
