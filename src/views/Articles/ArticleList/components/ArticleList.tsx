import React from 'react';

import { useGetArticles } from '../../hooks/useGetArticles';
import useGetFavouriteArticles from '../../hooks/useGetFavouriteArticles';
import type { ArticleListProps } from '../types';

import ArticleListFooter from './ArticleListFooter';
import ArticleListSearch from './ArticleListSearch';
import ArticleListSorting from './ArticleListSorting';
import VirtualizedTable from './VirtualizedTable';

const FavouriteArticles = ({
  data,
  paginationData,
  articleQuery,
  setArticleQuery,
}: ArticleListProps) => {
  const setArticleQueryProps = { articleQuery, setArticleQuery };
  const { isLoading: isFavouriteArticlesLoading } = useGetFavouriteArticles();
  const { isLoading: isArticlesLoading } = useGetArticles();

  const isSomeArticlesLoading = isFavouriteArticlesLoading || isArticlesLoading;

  return (
    <div className='block'>
      <div className='block-inner'>
        <div className='block-indent'>
          <div className={'table-control'}>
            <ArticleListSearch {...setArticleQueryProps} />
            <ArticleListSorting {...setArticleQueryProps} />
          </div>

          {isSomeArticlesLoading && 'loading...'}
          {data && <VirtualizedTable data={data} />}

          <ArticleListFooter {...setArticleQueryProps} paginationData={paginationData} />
        </div>
      </div>
    </div>
  );
};

export default FavouriteArticles;
