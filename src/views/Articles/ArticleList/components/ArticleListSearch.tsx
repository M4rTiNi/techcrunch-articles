import { debounce } from 'lodash';
import React, { useCallback, useEffect, useMemo } from 'react';

import type { QueryType } from '../types';

const ArticleListSearch = ({ articleQuery, setArticleQuery }: QueryType) => {
  const changeHandler = useCallback(
    (event: React.ChangeEvent<HTMLInputElement>) => {
      setArticleQuery({
        ...articleQuery,
        search: event.target.value,
      });
    },
    [articleQuery, setArticleQuery],
  );

  const debouncedSearch = useMemo(() => debounce(changeHandler, 500), [changeHandler]);

  useEffect(() => {
    return () => debouncedSearch.cancel();
  }, [debouncedSearch]);

  return (
    <div className={'table-control__search'}>
      <label htmlFor={'search-input'}>search:</label>
      <input
        type={'text'}
        name={'search-input'}
        defaultValue={articleQuery.search}
        onChange={debouncedSearch}
      />
    </div>
  );
};

export default ArticleListSearch;
