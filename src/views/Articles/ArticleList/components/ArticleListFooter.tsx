import React, { useState } from 'react';

import type { PaginationDataType } from '../../../../api';
import type { PerPageType } from '../../../../state/type';
import { ItemsPerPage } from '../../../../state/type';
import type { QueryType } from '../types';

import Pagination from './Pagination';

const itemsPerPage: ItemsPerPage[] = [
  ItemsPerPage.twenty,
  ItemsPerPage.fifty,
  ItemsPerPage.hundred,
];

const ArticleListFooter = ({
  articleQuery,
  setArticleQuery,
  paginationData,
}: QueryType & PaginationDataType) => {
  const [currentPage, setCurrentPage] = useState<number>(1);

  return (
    <div className={'pagination'}>
      <div className={'pagination__perView'}>
        <label htmlFor={'per_page'}>per page:</label>
        <select
          name='per_page'
          id='per_page'
          value={articleQuery.per_page}
          onChange={(event) =>
            setArticleQuery({
              ...articleQuery,
              per_page: parseInt(event.target.value) as PerPageType,
            })
          }
        >
          {itemsPerPage.map((option) => (
            <option key={option}>{option}</option>
          ))}
        </select>
      </div>

      {paginationData?.wpTotalArticles && articleQuery.per_page && (
        <Pagination
          onPageChange={(page) => {
            setArticleQuery({
              ...articleQuery,
              page: page,
            });

            setCurrentPage(page);
          }}
          currentPage={currentPage}
          totalCount={paginationData?.wpTotalArticles}
          pageSize={articleQuery.per_page}
        />
      )}
    </div>
  );
};

export default ArticleListFooter;
