import { useWindowVirtualizer } from '@tanstack/react-virtual';
import { xor } from 'lodash';
import React from 'react';
import { useRecoilState } from 'recoil';

import { favouriteArticleQueryKeysState } from '../../../../state';
import { useGetArticles } from '../../hooks/useGetArticles';
import type { ArticleItemData } from '../types';

const FavouriteButton = ({ id }: { id: number }) => {
  const [favouriteArticleQuery, setFavouriteArticleQuery] = useRecoilState(
    favouriteArticleQueryKeysState,
  );
  const isInFavourite = favouriteArticleQuery?.include?.includes(id);

  return (
    <button
      style={{ width: '165px' }}
      type={'button'}
      onClick={() =>
        setFavouriteArticleQuery({
          ...favouriteArticleQuery,
          include: xor(favouriteArticleQuery?.include, [id]),
        })
      }
    >
      {isInFavourite ? 'remove from favourites' : 'add to favourites'}
    </button>
  );
};

type RowVirtualizerFixedProps = {
  data: ArticleItemData[];
  rowControl?: React.FC;
};

const VirtualizedTable = ({ data }: RowVirtualizerFixedProps) => {
  const { isFetching: isArticlesFetching } = useGetArticles();

  const rowVirtualizer = useWindowVirtualizer({
    count: data.length,
    getScrollElement: () => window,
    overscan: 10,
    estimateSize: () => 30,
  });

  const virtualRows = rowVirtualizer.getVirtualItems();
  const totalSize = rowVirtualizer.getTotalSize();

  const paddingTop = virtualRows.length > 0 ? virtualRows?.[0]?.start || 0 : 0;
  const paddingBottom =
    virtualRows.length > 0 ? totalSize - (virtualRows?.[virtualRows.length - 1]?.end || 0) : 0;

  return (
    <div className={'table-wrapper'}>
      {isArticlesFetching && <div className={'loading-indicator'} />}
      <table className={'table-body'}>
        <thead>
          <tr>
            <th>ID</th>
            <th>Title</th>
            <th>Date</th>
            <th>Link</th>
            <th>Author</th>
            <th />
          </tr>
        </thead>
        <tbody>
          {paddingTop > 0 && (
            <tr>
              <td style={{ height: `${paddingTop}px` }} />
            </tr>
          )}
          {rowVirtualizer.getVirtualItems().map((virtualRow) => {
            const rowData: ArticleItemData = data[virtualRow.index];
            return (
              <tr key={virtualRow.index}>
                <td>{rowData.id}</td>
                <td dangerouslySetInnerHTML={{ __html: rowData.title.rendered }} />
                <td
                  dangerouslySetInnerHTML={{
                    __html: new Date(rowData.date)
                      .toLocaleDateString('cs-CZ')
                      .replaceAll(' ', '&nbsp;'),
                  }}
                />
                <td>
                  <a href={rowData.link} rel='noreferrer' target={'_blank'}>
                    open
                  </a>
                </td>
                <td>{rowData.author}</td>
                <td>
                  <FavouriteButton id={rowData.id} />
                </td>
              </tr>
            );
          })}
          {paddingBottom > 0 && (
            <tr>
              <td style={{ height: `${paddingBottom}px` }} />
            </tr>
          )}
        </tbody>
      </table>
    </div>
  );
};

export default VirtualizedTable;
