import React from 'react';

import { OrderByEnum } from '../../../../state/type';
import type { QueryType } from '../types';

const sortValues: OrderByEnum[] = [OrderByEnum.date, OrderByEnum.author, OrderByEnum.title];

const ArticleListSorting = ({ articleQuery, setArticleQuery }: QueryType) => {
  return (
    <div className={'table-control__sorting'}>
      <label htmlFor='sort'>sort by:</label>
      <select
        id={'sort'}
        name={'sort'}
        value={articleQuery.orderby}
        onChange={(event) =>
          setArticleQuery({
            ...articleQuery,
            orderby: event.target.value as OrderByEnum,
          })
        }
      >
        {sortValues.map((option) => (
          <option key={option} value={option}>
            {option}
          </option>
        ))}
      </select>
    </div>
  );
};

export default ArticleListSorting;
