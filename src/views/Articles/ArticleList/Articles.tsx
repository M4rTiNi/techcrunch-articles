import React from 'react';
import { useRecoilState } from 'recoil';

import Layout from '../../../components/Layout';
import { articleQueryState } from '../../../state';
import { useGetArticles } from '../hooks/useGetArticles';

import ArticleList from './components/ArticleList';

const FavouriteArticles = () => {
  const [articleQueryKeys, setArticleQueryKeys] = useRecoilState(articleQueryState);
  const { data } = useGetArticles();

  return (
    <Layout>
      <ArticleList
        data={data?.data}
        paginationData={data?.paginationData}
        articleQuery={articleQueryKeys}
        setArticleQuery={setArticleQueryKeys}
      />
    </Layout>
  );
};

export default FavouriteArticles;
