import { useQuery } from '@tanstack/react-query';
import { useRecoilValue } from 'recoil';

import { getArticles } from '../../../api';
import { favouriteArticleQueryKeysState } from '../../../state';

const UseGetFavouriteArticles = () => {
  const query = useRecoilValue(favouriteArticleQueryKeysState);

  return useQuery({
    queryKey: ['favourite-articles', query],
    queryFn: () => getArticles(query),
    keepPreviousData: true,
    staleTime: 5 * 60 * 1000,
  });
};

export default UseGetFavouriteArticles;
