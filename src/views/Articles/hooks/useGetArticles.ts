import { useQuery, useQueryClient } from '@tanstack/react-query';
import { useEffect } from 'react';
import { useRecoilValue } from 'recoil';

import { getArticles } from '../../../api';
import { articleQueryState } from '../../../state';

export const useGetArticles = () => {
  const query = useRecoilValue(articleQueryState);

  const queryClient = useQueryClient();

  useEffect(() => {
    const nextPageQuery = { ...query, page: query.page + 1 };
    const prefetchNextPage = async () => {
      await queryClient.prefetchQuery({
        queryKey: ['articles', nextPageQuery],
        queryFn: () => getArticles(nextPageQuery),
      });
    };
    prefetchNextPage();
  }, [query.page]);

  return useQuery({
    queryKey: ['articles', query],
    queryFn: () => getArticles(query),
    keepPreviousData: true,
    staleTime: 5 * 60 * 1000,
  });
};
