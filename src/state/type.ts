export enum ItemsPerPage {
  twenty = 20,
  fifty = 50,
  hundred = 100,
}
export enum OrderByEnum {
  author = 'author',
  date = 'date',
  title = 'title',
}
export type PerPageType = ItemsPerPage.twenty | ItemsPerPage.fifty | ItemsPerPage.hundred;
export type OrderByType = OrderByEnum.author | OrderByEnum.date | OrderByEnum.title;

export type ArticleQueryParams = {
  per_page?: PerPageType;
  search?: string;
  orderby?: OrderByType;
  include?: number[];
  page: number;
};
