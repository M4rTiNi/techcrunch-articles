import type { AtomEffect } from 'recoil';
import { atom, DefaultValue } from 'recoil';

import type { ArticleQueryParams } from './type';
import { OrderByEnum } from './type';

export enum atomKeyEnum {
  articleQueryState = 'article-query-state',
  favouriteArticleQueryKeysState = 'favourite-article-keys-query',
}

export const defaultArticleParams: ArticleQueryParams = {
  per_page: 20,
  search: '',
  orderby: OrderByEnum.date,
  page: 1,
};

const localStorageEffect: <T>(key: string) => AtomEffect<T> =
  (key: string) =>
  ({ setSelf, onSet }) => {
    const savedValue = localStorage.getItem(key);
    if (savedValue != null) {
      setSelf(JSON.parse(savedValue));
    }

    onSet((newValue) => {
      if (newValue instanceof DefaultValue) {
        localStorage.removeItem(key);
      } else {
        localStorage.setItem(key, JSON.stringify(newValue));
      }
    });
  };

export const articleQueryState = atom({
  key: atomKeyEnum.articleQueryState,
  default: defaultArticleParams as ArticleQueryParams,
});

export const favouriteArticleQueryKeysState = atom({
  key: atomKeyEnum.favouriteArticleQueryKeysState,
  default: defaultArticleParams as ArticleQueryParams,
  effects: [localStorageEffect(atomKeyEnum.favouriteArticleQueryKeysState)],
});
