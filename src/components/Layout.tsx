import { ReactQueryDevtools } from '@tanstack/react-query-devtools';
import React from 'react';
import { NavLink } from 'react-router-dom';

import { ROUTESEnum } from '../const';

const navLinks: {
  path: ROUTESEnum;
  text: string;
}[] = [
  {
    path: ROUTESEnum.homepage,
    text: 'Article List',
  },
  {
    path: ROUTESEnum.favourites,
    text: 'Favourite Articles',
  },
];

type LayoutPropsType = {
  children: React.ReactNode;
};

const Layout: React.FC<LayoutPropsType> = ({ children }) => {
  return (
    <div className={'app'}>
      <header className={'block header'}>
        <div className={'block-inner'}>
          <h1>TC articles</h1>
          <nav>
            {navLinks.map((link) => {
              return (
                <NavLink
                  key={link.path}
                  to={link.path}
                  className={({ isActive }) => (isActive ? 'active' : undefined)}
                >
                  {link.text}
                </NavLink>
              );
            })}
          </nav>
        </div>
      </header>
      <br />
      <br />
      {children}
      <br />
      <br />
      <br />
      <ReactQueryDevtools initialIsOpen={false} />
    </div>
  );
};

export default Layout;
