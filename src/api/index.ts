import axios from 'axios';

import type { ArticleQueryParams } from '../state/type';
import type { ArticleItemData } from '../views/Articles/ArticleList/types';

type ArticlePromiseReturnType = {
  data: ArticleItemData[];
  headers: {
    ['x-wp-total']: number;
    ['x-wp-totalpages']: number;
  };
};

export type PaginationDataType = {
  paginationData?: {
    wpTotalArticles: number | undefined;
    wpTotalPages: number | undefined;
  };
};

export type GetArticlesReturnType = {
  data: ArticleItemData[] | undefined;
} & PaginationDataType;

export const getArticles = async (params: ArticleQueryParams): Promise<GetArticlesReturnType> => {
  const { data, headers }: ArticlePromiseReturnType = await axios.get(
    `https://techcrunch.com/wp-json/wp/v2/posts`,
    {
      params: params,
    },
  );

  return {
    data: data,
    paginationData: {
      wpTotalArticles: headers['x-wp-total'],
      wpTotalPages: headers['x-wp-totalpages'],
    },
  };
};
