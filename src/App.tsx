import { QueryClient, QueryClientProvider } from '@tanstack/react-query';
import React from 'react';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import './App.sass';
import { RecoilRoot } from 'recoil';

import { ROUTESEnum } from './const';
import Articles from './views/Articles/ArticleList/Articles';
import FavouriteArticles from './views/Articles/ArticleList/FavouriteArticles';

const queryClient = new QueryClient();

const routes: {
  path: ROUTESEnum;
  component: React.FC;
}[] = [
  {
    path: ROUTESEnum.homepage,
    component: Articles,
  },
  {
    path: ROUTESEnum.favourites,
    component: FavouriteArticles,
  },
];

const App = () => {
  return (
    <RecoilRoot>
      <QueryClientProvider client={queryClient}>
        <BrowserRouter>
          <Routes>
            {routes.map((route) => {
              return <Route key={route.path} path={route.path} element={<route.component />} />;
            })}
          </Routes>
        </BrowserRouter>
      </QueryClientProvider>
    </RecoilRoot>
  );
};

export default App;
