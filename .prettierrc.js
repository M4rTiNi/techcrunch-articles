module.exports = {
  singleQuote: true,
  jsxSingleQuote: true,
  semi: true,
  tabWidth: 2,
  printWidth: 100,
  trailingComma: "all",
  endOfLine: "auto",
};
